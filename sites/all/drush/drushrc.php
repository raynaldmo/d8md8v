<?php 


$options['sites'] = array (
);
$options['profiles'] = array (
  0 => 'standard',
);
$options['packages'] = array (
  'base' => 
  array (
    'modules' => 
    array (
      'workflows' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/workflows/workflows.module',
        'basename' => 'workflows.module',
        'name' => 'workflows',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/workflows/workflows.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'views_ui_test_field' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/views_ui/tests/modules/views_ui_test_field/views_ui_test_field.module',
        'basename' => 'views_ui_test_field.module',
        'name' => 'views_ui_test_field',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/views_ui/tests/modules/views_ui_test_field/views_ui_test_field.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'views_ui_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/views_ui/tests/modules/views_ui_test/views_ui_test.module',
        'basename' => 'views_ui_test.module',
        'name' => 'views_ui_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/views_ui/tests/modules/views_ui_test/views_ui_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'views_ui' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/views_ui/views_ui.module',
        'basename' => 'views_ui.module',
        'name' => 'views_ui',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/views_ui/views_ui.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'views_test_data' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/views/tests/modules/views_test_data/views_test_data.module',
        'basename' => 'views_test_data.module',
        'name' => 'views_test_data',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/views/tests/modules/views_test_data/views_test_data.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'views_entity_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/views/tests/modules/views_entity_test/views_entity_test.module',
        'basename' => 'views_entity_test.module',
        'name' => 'views_entity_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/views/tests/modules/views_entity_test/views_entity_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'views' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/views/views.module',
        'basename' => 'views.module',
        'name' => 'views',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/views/views.info.yml',
        'schema_version' => '8201',
        'version' => '/',
      ),
      'user_hooks_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/user/tests/modules/user_hooks_test/user_hooks_test.module',
        'basename' => 'user_hooks_test.module',
        'name' => 'user_hooks_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/user/tests/modules/user_hooks_test/user_hooks_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'user_form_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/user/tests/modules/user_form_test/user_form_test.module',
        'basename' => 'user_form_test.module',
        'name' => 'user_form_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/user/tests/modules/user_form_test/user_form_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'user_access_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/user/tests/modules/user_access_test/user_access_test.module',
        'basename' => 'user_access_test.module',
        'name' => 'user_access_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/user/tests/modules/user_access_test/user_access_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'user' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/user/user.module',
        'basename' => 'user.module',
        'name' => 'user',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/user/user.info.yml',
        'schema_version' => '8100',
        'version' => '/',
      ),
      'update_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/update/tests/modules/update_test/update_test.module',
        'basename' => 'update_test.module',
        'name' => 'update_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/update/tests/modules/update_test/update_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'update' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/update/update.module',
        'basename' => 'update.module',
        'name' => 'update',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/update/update.info.yml',
        'schema_version' => '8001',
        'version' => '/',
      ),
      'tracker' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/tracker/tracker.module',
        'basename' => 'tracker.module',
        'name' => 'tracker',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/tracker/tracker.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'tour_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/tour/tests/tour_test/tour_test.module',
        'basename' => 'tour_test.module',
        'name' => 'tour_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/tour/tests/tour_test/tour_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'tour' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/tour/tour.module',
        'basename' => 'tour.module',
        'name' => 'tour',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/tour/tour.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'toolbar_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/toolbar/tests/modules/toolbar_test/toolbar_test.module',
        'basename' => 'toolbar_test.module',
        'name' => 'toolbar_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/toolbar/tests/modules/toolbar_test/toolbar_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'toolbar_disable_user_toolbar' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/toolbar/tests/modules/toolbar_disable_user_toolbar/toolbar_disable_user_toolbar.module',
        'basename' => 'toolbar_disable_user_toolbar.module',
        'name' => 'toolbar_disable_user_toolbar',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/toolbar/tests/modules/toolbar_disable_user_toolbar/toolbar_disable_user_toolbar.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'toolbar' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/toolbar/toolbar.module',
        'basename' => 'toolbar.module',
        'name' => 'toolbar',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/toolbar/toolbar.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'text' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/text/text.module',
        'basename' => 'text.module',
        'name' => 'text',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/text/text.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'telephone' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/telephone/telephone.module',
        'basename' => 'telephone.module',
        'name' => 'telephone',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/telephone/telephone.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'taxonomy_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/taxonomy/tests/modules/taxonomy_test/taxonomy_test.module',
        'basename' => 'taxonomy_test.module',
        'name' => 'taxonomy_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/taxonomy/tests/modules/taxonomy_test/taxonomy_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'taxonomy_crud' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/taxonomy/tests/modules/taxonomy_crud/taxonomy_crud.module',
        'basename' => 'taxonomy_crud.module',
        'name' => 'taxonomy_crud',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/taxonomy/tests/modules/taxonomy_crud/taxonomy_crud.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'taxonomy' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/taxonomy/taxonomy.module',
        'basename' => 'taxonomy.module',
        'name' => 'taxonomy',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/taxonomy/taxonomy.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'update_script_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/update_script_test/update_script_test.module',
        'basename' => 'update_script_test.module',
        'name' => 'update_script_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/update_script_test/update_script_test.info.yml',
        'schema_version' => '8001',
        'version' => '/',
      ),
      'twig_theme_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/twig_theme_test/twig_theme_test.module',
        'basename' => 'twig_theme_test.module',
        'name' => 'twig_theme_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/twig_theme_test/twig_theme_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'twig_extension_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/twig_extension_test/twig_extension_test.module',
        'basename' => 'twig_extension_test.module',
        'name' => 'twig_extension_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/twig_extension_test/twig_extension_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'theme_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/theme_test/theme_test.module',
        'basename' => 'theme_test.module',
        'name' => 'theme_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/theme_test/theme_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'theme_suggestions_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/theme_suggestions_test/theme_suggestions_test.module',
        'basename' => 'theme_suggestions_test.module',
        'name' => 'theme_suggestions_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/theme_suggestions_test/theme_suggestions_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'theme_region_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/theme_region_test/theme_region_test.module',
        'basename' => 'theme_region_test.module',
        'name' => 'theme_region_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/theme_region_test/theme_region_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'theme_page_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/theme_page_test/theme_page_test.module',
        'basename' => 'theme_page_test.module',
        'name' => 'theme_page_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/theme_page_test/theme_page_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'system_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/system_test/system_test.module',
        'basename' => 'system_test.module',
        'name' => 'system_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/system_test/system_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'system_module_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/system_module_test/system_module_test.module',
        'basename' => 'system_module_test.module',
        'name' => 'system_module_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/system_module_test/system_module_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'session_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/session_test/session_test.module',
        'basename' => 'session_test.module',
        'name' => 'session_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/session_test/session_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'session_exists_cache_context_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/session_exists_cache_context_test/session_exists_cache_context_test.module',
        'basename' => 'session_exists_cache_context_test.module',
        'name' => 'session_exists_cache_context_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/session_exists_cache_context_test/session_exists_cache_context_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'plugin_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/plugin_test/plugin_test.module',
        'basename' => 'plugin_test.module',
        'name' => 'plugin_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/plugin_test/plugin_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'path_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/path_test/path_test.module',
        'basename' => 'path_test.module',
        'name' => 'path_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/path_test/path_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'module_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/module_test/module_test.module',
        'basename' => 'module_test.module',
        'name' => 'module_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/module_test/module_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'module_required_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/module_required_test/module_required_test.module',
        'basename' => 'module_required_test.module',
        'name' => 'module_required_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/module_required_test/module_required_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'menu_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/menu_test/menu_test.module',
        'basename' => 'menu_test.module',
        'name' => 'menu_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/menu_test/menu_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'link_generation_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/link_generation_test/link_generation_test.module',
        'basename' => 'link_generation_test.module',
        'name' => 'link_generation_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/link_generation_test/link_generation_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'layout_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/layout_test/layout_test.module',
        'basename' => 'layout_test.module',
        'name' => 'layout_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/layout_test/layout_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'keyvalue_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/keyvalue_test/keyvalue_test.module',
        'basename' => 'keyvalue_test.module',
        'name' => 'keyvalue_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/keyvalue_test/keyvalue_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'form_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/form_test/form_test.module',
        'basename' => 'form_test.module',
        'name' => 'form_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/form_test/form_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'experimental_module_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/experimental_module_test/experimental_module_test.module',
        'basename' => 'experimental_module_test.module',
        'name' => 'experimental_module_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/experimental_module_test/experimental_module_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'experimental_module_requirements_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/experimental_module_requirements_test/experimental_module_requirements_test.module',
        'basename' => 'experimental_module_requirements_test.module',
        'name' => 'experimental_module_requirements_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/experimental_module_requirements_test/experimental_module_requirements_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'experimental_module_dependency_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/experimental_module_dependency_test/experimental_module_dependency_test.module',
        'basename' => 'experimental_module_dependency_test.module',
        'name' => 'experimental_module_dependency_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/experimental_module_dependency_test/experimental_module_dependency_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'entity_test_update' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/entity_test_update/entity_test_update.module',
        'basename' => 'entity_test_update.module',
        'name' => 'entity_test_update',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/entity_test_update/entity_test_update.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'entity_test_operation' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/entity_test_operation/entity_test_operation.module',
        'basename' => 'entity_test_operation.module',
        'name' => 'entity_test_operation',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/entity_test_operation/entity_test_operation.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'entity_test_extra' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/entity_test_extra/entity_test_extra.module',
        'basename' => 'entity_test_extra.module',
        'name' => 'entity_test_extra',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/entity_test_extra/entity_test_extra.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'entity_test_constraints' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/entity_test_constraints/entity_test_constraints.module',
        'basename' => 'entity_test_constraints.module',
        'name' => 'entity_test_constraints',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/entity_test_constraints/entity_test_constraints.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'entity_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/entity_test/entity_test.module',
        'basename' => 'entity_test.module',
        'name' => 'entity_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/entity_test/entity_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'entity_schema_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/entity_schema_test/entity_schema_test.module',
        'basename' => 'entity_schema_test.module',
        'name' => 'entity_schema_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/entity_schema_test/entity_schema_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'entity_reference_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/entity_reference_test/entity_reference_test.module',
        'basename' => 'entity_reference_test.module',
        'name' => 'entity_reference_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/entity_reference_test/entity_reference_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'entity_crud_hook_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/entity_crud_hook_test/entity_crud_hook_test.module',
        'basename' => 'entity_crud_hook_test.module',
        'name' => 'entity_crud_hook_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/entity_crud_hook_test/entity_crud_hook_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'database_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/database_test/database_test.module',
        'basename' => 'database_test.module',
        'name' => 'database_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/database_test/database_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'common_test_cron_helper' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/common_test_cron_helper/common_test_cron_helper.module',
        'basename' => 'common_test_cron_helper.module',
        'name' => 'common_test_cron_helper',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/common_test_cron_helper/common_test_cron_helper.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'common_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/common_test/common_test.module',
        'basename' => 'common_test.module',
        'name' => 'common_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/common_test/common_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'batch_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/batch_test/batch_test.module',
        'basename' => 'batch_test.module',
        'name' => 'batch_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/batch_test/batch_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'ajax_forms_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/ajax_forms_test/ajax_forms_test.module',
        'basename' => 'ajax_forms_test.module',
        'name' => 'ajax_forms_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/tests/modules/ajax_forms_test/ajax_forms_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'system' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/system/system.module',
        'basename' => 'system.module',
        'name' => 'system',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/system/system.info.yml',
        'schema_version' => '8401',
        'version' => '/',
      ),
      'syslog' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/syslog/syslog.module',
        'basename' => 'syslog.module',
        'name' => 'syslog',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/syslog/syslog.info.yml',
        'schema_version' => '8400',
        'version' => '/',
      ),
      'statistics' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/statistics/statistics.module',
        'basename' => 'statistics.module',
        'name' => 'statistics',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/statistics/statistics.info.yml',
        'schema_version' => '8300',
        'version' => '/',
      ),
      'simpletest' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/simpletest/simpletest.module',
        'basename' => 'simpletest.module',
        'name' => 'simpletest',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/simpletest/simpletest.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'shortcut' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/shortcut/shortcut.module',
        'basename' => 'shortcut.module',
        'name' => 'shortcut',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/shortcut/shortcut.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'entity_serialization_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/serialization/tests/modules/entity_serialization_test/entity_serialization_test.module',
        'basename' => 'entity_serialization_test.module',
        'name' => 'entity_serialization_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/serialization/tests/modules/entity_serialization_test/entity_serialization_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'serialization' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/serialization/serialization.module',
        'basename' => 'serialization.module',
        'name' => 'serialization',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/serialization/serialization.info.yml',
        'schema_version' => '8401',
        'version' => '/',
      ),
      'search_query_alter' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/search/tests/modules/search_query_alter/search_query_alter.module',
        'basename' => 'search_query_alter.module',
        'name' => 'search_query_alter',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/search/tests/modules/search_query_alter/search_query_alter.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'search_langcode_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/search/tests/modules/search_langcode_test/search_langcode_test.module',
        'basename' => 'search_langcode_test.module',
        'name' => 'search_langcode_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/search/tests/modules/search_langcode_test/search_langcode_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'search_embedded_form' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/search/tests/modules/search_embedded_form/search_embedded_form.module',
        'basename' => 'search_embedded_form.module',
        'name' => 'search_embedded_form',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/search/tests/modules/search_embedded_form/search_embedded_form.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'search_date_query_alter' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/search/tests/modules/search_date_query_alter/search_date_query_alter.module',
        'basename' => 'search_date_query_alter.module',
        'name' => 'search_date_query_alter',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/search/tests/modules/search_date_query_alter/search_date_query_alter.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'search' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/search/search.module',
        'basename' => 'search.module',
        'name' => 'search',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/search/search.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'rest_test_views' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/rest/tests/modules/rest_test_views/rest_test_views.module',
        'basename' => 'rest_test_views.module',
        'name' => 'rest_test_views',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/rest/tests/modules/rest_test_views/rest_test_views.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'rest_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/rest/tests/modules/rest_test/rest_test.module',
        'basename' => 'rest_test.module',
        'name' => 'rest_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/rest/tests/modules/rest_test/rest_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'config_test_rest' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/rest/tests/modules/config_test_rest/config_test_rest.module',
        'basename' => 'config_test_rest.module',
        'name' => 'config_test_rest',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/rest/tests/modules/config_test_rest/config_test_rest.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'rest' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/rest/rest.module',
        'basename' => 'rest.module',
        'name' => 'rest',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/rest/rest.info.yml',
        'schema_version' => '8203',
        'version' => '/',
      ),
      'responsive_image' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/responsive_image/responsive_image.module',
        'basename' => 'responsive_image.module',
        'name' => 'responsive_image',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/responsive_image/responsive_image.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'rdf_test_namespaces' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/rdf/tests/rdf_test_namespaces/rdf_test_namespaces.module',
        'basename' => 'rdf_test_namespaces.module',
        'name' => 'rdf_test_namespaces',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/rdf/tests/rdf_test_namespaces/rdf_test_namespaces.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'rdf_conflicting_namespaces' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/rdf/tests/rdf_conflicting_namespaces/rdf_conflicting_namespaces.module',
        'basename' => 'rdf_conflicting_namespaces.module',
        'name' => 'rdf_conflicting_namespaces',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/rdf/tests/rdf_conflicting_namespaces/rdf_conflicting_namespaces.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'rdf' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/rdf/rdf.module',
        'basename' => 'rdf.module',
        'name' => 'rdf',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/rdf/rdf.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'quickedit_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/quickedit/tests/modules/quickedit_test.module',
        'basename' => 'quickedit_test.module',
        'name' => 'quickedit_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/quickedit/tests/modules/quickedit_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'quickedit' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/quickedit/quickedit.module',
        'basename' => 'quickedit.module',
        'name' => 'quickedit',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/quickedit/quickedit.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'path' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/path/path.module',
        'basename' => 'path.module',
        'name' => 'path',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/path/path.info.yml',
        'schema_version' => '8200',
        'version' => '/',
      ),
      'page_cache_form_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/page_cache/tests/modules/page_cache_form_test.module',
        'basename' => 'page_cache_form_test.module',
        'name' => 'page_cache_form_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/page_cache/tests/modules/page_cache_form_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'page_cache' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/page_cache/page_cache.module',
        'basename' => 'page_cache.module',
        'name' => 'page_cache',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/page_cache/page_cache.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'outside_in_test_css' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/outside_in/tests/modules/outside_in_test_css/outside_in_test_css.module',
        'basename' => 'outside_in_test_css.module',
        'name' => 'outside_in_test_css',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/outside_in/tests/modules/outside_in_test_css/outside_in_test_css.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'outside_in' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/outside_in/outside_in.module',
        'basename' => 'outside_in.module',
        'name' => 'outside_in',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/outside_in/outside_in.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'options_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/options/tests/options_test/options_test.module',
        'basename' => 'options_test.module',
        'name' => 'options_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/options/tests/options_test/options_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'options' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/options/options.module',
        'basename' => 'options.module',
        'name' => 'options',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/options/options.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'node_test_exception' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/node/tests/modules/node_test_exception/node_test_exception.module',
        'basename' => 'node_test_exception.module',
        'name' => 'node_test_exception',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/node/tests/modules/node_test_exception/node_test_exception.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'node_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/node/tests/modules/node_test/node_test.module',
        'basename' => 'node_test.module',
        'name' => 'node_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/node/tests/modules/node_test/node_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'node_access_test_language' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/node/tests/modules/node_access_test_language/node_access_test_language.module',
        'basename' => 'node_access_test_language.module',
        'name' => 'node_access_test_language',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/node/tests/modules/node_access_test_language/node_access_test_language.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'node_access_test_empty' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/node/tests/modules/node_access_test_empty/node_access_test_empty.module',
        'basename' => 'node_access_test_empty.module',
        'name' => 'node_access_test_empty',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/node/tests/modules/node_access_test_empty/node_access_test_empty.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'node_access_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/node/tests/modules/node_access_test/node_access_test.module',
        'basename' => 'node_access_test.module',
        'name' => 'node_access_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/node/tests/modules/node_access_test/node_access_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'node' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/node/node.module',
        'basename' => 'node.module',
        'name' => 'node',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/node/node.info.yml',
        'schema_version' => '8400',
        'version' => '/',
      ),
      'migrate_drupal_ui' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/migrate_drupal_ui/migrate_drupal_ui.module',
        'basename' => 'migrate_drupal_ui.module',
        'name' => 'migrate_drupal_ui',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/migrate_drupal_ui/migrate_drupal_ui.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'migrate_field_plugin_manager_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/migrate_drupal/tests/modules/migrate_field_plugin_manager_test/migrate_field_plugin_manager_test.module',
        'basename' => 'migrate_field_plugin_manager_test.module',
        'name' => 'migrate_field_plugin_manager_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/migrate_drupal/tests/modules/migrate_field_plugin_manager_test/migrate_field_plugin_manager_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'migrate_drupal' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/migrate_drupal/migrate_drupal.module',
        'basename' => 'migrate_drupal.module',
        'name' => 'migrate_drupal',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/migrate_drupal/migrate_drupal.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'migrate_prepare_row_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/migrate/tests/modules/migrate_prepare_row_test/migrate_prepare_row_test.module',
        'basename' => 'migrate_prepare_row_test.module',
        'name' => 'migrate_prepare_row_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/migrate/tests/modules/migrate_prepare_row_test/migrate_prepare_row_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'migrate' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/migrate/migrate.module',
        'basename' => 'migrate.module',
        'name' => 'migrate',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/migrate/migrate.info.yml',
        'schema_version' => '8001',
        'version' => '/',
      ),
      'menu_ui' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/menu_ui/menu_ui.module',
        'basename' => 'menu_ui.module',
        'name' => 'menu_ui',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/menu_ui/menu_ui.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'menu_link_content' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/menu_link_content/menu_link_content.module',
        'basename' => 'menu_link_content.module',
        'name' => 'menu_link_content',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/menu_link_content/menu_link_content.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'media' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/media/media.module',
        'basename' => 'media.module',
        'name' => 'media',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/media/media.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'locale_test_translate' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/locale/tests/modules/locale_test_translate/locale_test_translate.module',
        'basename' => 'locale_test_translate.module',
        'name' => 'locale_test_translate',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/locale/tests/modules/locale_test_translate/locale_test_translate.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'locale_test_development_release' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/locale/tests/modules/locale_test_development_release/locale_test_development_release.module',
        'basename' => 'locale_test_development_release.module',
        'name' => 'locale_test_development_release',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/locale/tests/modules/locale_test_development_release/locale_test_development_release.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'locale_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/locale/tests/modules/locale_test/locale_test.module',
        'basename' => 'locale_test.module',
        'name' => 'locale_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/locale/tests/modules/locale_test/locale_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'locale' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/locale/locale.module',
        'basename' => 'locale.module',
        'name' => 'locale',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/locale/locale.info.yml',
        'schema_version' => '8300',
        'version' => '/',
      ),
      'link' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/link/link.module',
        'basename' => 'link.module',
        'name' => 'link',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/link/link.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'layout_discovery' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/layout_discovery/layout_discovery.module',
        'basename' => 'layout_discovery.module',
        'name' => 'layout_discovery',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/layout_discovery/layout_discovery.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'language_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/language/tests/language_test/language_test.module',
        'basename' => 'language_test.module',
        'name' => 'language_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/language/tests/language_test/language_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'language' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/language/language.module',
        'basename' => 'language.module',
        'name' => 'language',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/language/language.info.yml',
        'schema_version' => '8001',
        'version' => '/',
      ),
      'inline_form_errors' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/inline_form_errors/inline_form_errors.module',
        'basename' => 'inline_form_errors.module',
        'name' => 'inline_form_errors',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/inline_form_errors/inline_form_errors.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'image_module_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/image/tests/modules/image_module_test/image_module_test.module',
        'basename' => 'image_module_test.module',
        'name' => 'image_module_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/image/tests/modules/image_module_test/image_module_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'image' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/image/image.module',
        'basename' => 'image.module',
        'name' => 'image',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/image/image.info.yml',
        'schema_version' => '8201',
        'version' => '/',
      ),
      'history' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/history/history.module',
        'basename' => 'history.module',
        'name' => 'history',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/history/history.info.yml',
        'schema_version' => '8101',
        'version' => '/',
      ),
      'more_help_page_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/help/tests/modules/more_help_page_test/more_help_page_test.module',
        'basename' => 'more_help_page_test.module',
        'name' => 'more_help_page_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/help/tests/modules/more_help_page_test/more_help_page_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'help_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/help/tests/modules/help_test/help_test.module',
        'basename' => 'help_test.module',
        'name' => 'help_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/help/tests/modules/help_test/help_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'help_page_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/help/tests/modules/help_page_test/help_page_test.module',
        'basename' => 'help_page_test.module',
        'name' => 'help_page_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/help/tests/modules/help_page_test/help_page_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'help' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/help/help.module',
        'basename' => 'help.module',
        'name' => 'help',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/help/help.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'hal_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/hal/tests/modules/hal_test/hal_test.module',
        'basename' => 'hal_test.module',
        'name' => 'hal_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/hal/tests/modules/hal_test/hal_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'hal' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/hal/hal.module',
        'basename' => 'hal.module',
        'name' => 'hal',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/hal/hal.info.yml',
        'schema_version' => '8301',
        'version' => '/',
      ),
      'forum' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/forum/forum.module',
        'basename' => 'forum.module',
        'name' => 'forum',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/forum/forum.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'filter_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/filter/tests/filter_test/filter_test.module',
        'basename' => 'filter_test.module',
        'name' => 'filter_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/filter/tests/filter_test/filter_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'filter' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/filter/filter.module',
        'basename' => 'filter.module',
        'name' => 'filter',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/filter/filter.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'file_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/file/tests/file_test/file_test.module',
        'basename' => 'file_test.module',
        'name' => 'file_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/file/tests/file_test/file_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'file' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/file/file.module',
        'basename' => 'file.module',
        'name' => 'file',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/file/file.info.yml',
        'schema_version' => '8300',
        'version' => '/',
      ),
      'field_ui_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/field_ui/tests/modules/field_ui_test/field_ui_test.module',
        'basename' => 'field_ui_test.module',
        'name' => 'field_ui_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/field_ui/tests/modules/field_ui_test/field_ui_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'field_ui' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/field_ui/field_ui.module',
        'basename' => 'field_ui.module',
        'name' => 'field_ui',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/field_ui/field_ui.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'field_layout_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/field_layout/tests/modules/field_layout_test/field_layout_test.module',
        'basename' => 'field_layout_test.module',
        'name' => 'field_layout_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/field_layout/tests/modules/field_layout_test/field_layout_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'field_layout' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/field_layout/field_layout.module',
        'basename' => 'field_layout.module',
        'name' => 'field_layout',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/field_layout/field_layout.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'field_third_party_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/field/tests/modules/field_third_party_test/field_third_party_test.module',
        'basename' => 'field_third_party_test.module',
        'name' => 'field_third_party_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/field/tests/modules/field_third_party_test/field_third_party_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'field_test_boolean_access_denied' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/field/tests/modules/field_test_boolean_access_denied/field_test_boolean_access_denied.module',
        'basename' => 'field_test_boolean_access_denied.module',
        'name' => 'field_test_boolean_access_denied',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/field/tests/modules/field_test_boolean_access_denied/field_test_boolean_access_denied.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'field_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/field/tests/modules/field_test/field_test.module',
        'basename' => 'field_test.module',
        'name' => 'field_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/field/tests/modules/field_test/field_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'field' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/field/field.module',
        'basename' => 'field.module',
        'name' => 'field',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/field/field.info.yml',
        'schema_version' => '8003',
        'version' => '/',
      ),
      'entity_reference' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/entity_reference/entity_reference.module',
        'basename' => 'entity_reference.module',
        'name' => 'entity_reference',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/entity_reference/entity_reference.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'editor_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/editor/tests/modules/editor_test.module',
        'basename' => 'editor_test.module',
        'name' => 'editor_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/editor/tests/modules/editor_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'editor' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/editor/editor.module',
        'basename' => 'editor.module',
        'name' => 'editor',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/editor/editor.info.yml',
        'schema_version' => '8001',
        'version' => '/',
      ),
      'dynamic_page_cache' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/dynamic_page_cache/dynamic_page_cache.module',
        'basename' => 'dynamic_page_cache.module',
        'name' => 'dynamic_page_cache',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/dynamic_page_cache/dynamic_page_cache.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'dblog' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/dblog/dblog.module',
        'basename' => 'dblog.module',
        'name' => 'dblog',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/dblog/dblog.info.yml',
        'schema_version' => '8400',
        'version' => '/',
      ),
      'datetime_range' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/datetime_range/datetime_range.module',
        'basename' => 'datetime_range.module',
        'name' => 'datetime_range',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/datetime_range/datetime_range.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'datetime' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/datetime/datetime.module',
        'basename' => 'datetime.module',
        'name' => 'datetime',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/datetime/datetime.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'contextual' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/contextual/contextual.module',
        'basename' => 'contextual.module',
        'name' => 'contextual',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/contextual/contextual.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'content_translation_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/content_translation/tests/modules/content_translation_test/content_translation_test.module',
        'basename' => 'content_translation_test.module',
        'name' => 'content_translation_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/content_translation/tests/modules/content_translation_test/content_translation_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'content_translation' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/content_translation/content_translation.module',
        'basename' => 'content_translation.module',
        'name' => 'content_translation',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/content_translation/content_translation.info.yml',
        'schema_version' => '8400',
        'version' => '/',
      ),
      'content_moderation' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/content_moderation/content_moderation.module',
        'basename' => 'content_moderation.module',
        'name' => 'content_moderation',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/content_moderation/content_moderation.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'contact_storage_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/contact/tests/modules/contact_storage_test/contact_storage_test.module',
        'basename' => 'contact_storage_test.module',
        'name' => 'contact_storage_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/contact/tests/modules/contact_storage_test/contact_storage_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'contact' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/contact/contact.module',
        'basename' => 'contact.module',
        'name' => 'contact',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/contact/contact.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'config_translation_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/config_translation/tests/modules/config_translation_test/config_translation_test.module',
        'basename' => 'config_translation_test.module',
        'name' => 'config_translation_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/config_translation/tests/modules/config_translation_test/config_translation_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'config_translation' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/config_translation/config_translation.module',
        'basename' => 'config_translation.module',
        'name' => 'config_translation',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/config_translation/config_translation.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'config_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/config/tests/config_test/config_test.module',
        'basename' => 'config_test.module',
        'name' => 'config_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/config/tests/config_test/config_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'config_schema_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/config/tests/config_schema_test/config_schema_test.module',
        'basename' => 'config_schema_test.module',
        'name' => 'config_schema_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/config/tests/config_schema_test/config_schema_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'config_install_dependency_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/config/tests/config_install_dependency_test/config_install_dependency_test.module',
        'basename' => 'config_install_dependency_test.module',
        'name' => 'config_install_dependency_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/config/tests/config_install_dependency_test/config_install_dependency_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'config_import_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/config/tests/config_import_test/config_import_test.module',
        'basename' => 'config_import_test.module',
        'name' => 'config_import_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/config/tests/config_import_test/config_import_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'config_entity_static_cache_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/config/tests/config_entity_static_cache_test/config_entity_static_cache_test.module',
        'basename' => 'config_entity_static_cache_test.module',
        'name' => 'config_entity_static_cache_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/config/tests/config_entity_static_cache_test/config_entity_static_cache_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'config' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/config/config.module',
        'basename' => 'config.module',
        'name' => 'config',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/config/config.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'comment_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/comment/tests/modules/comment_test/comment_test.module',
        'basename' => 'comment_test.module',
        'name' => 'comment_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/comment/tests/modules/comment_test/comment_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'comment_empty_title_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/comment/tests/modules/comment_empty_title_test/comment_empty_title_test.module',
        'basename' => 'comment_empty_title_test.module',
        'name' => 'comment_empty_title_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/comment/tests/modules/comment_empty_title_test/comment_empty_title_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'comment' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/comment/comment.module',
        'basename' => 'comment.module',
        'name' => 'comment',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/comment/comment.info.yml',
        'schema_version' => '8400',
        'version' => '/',
      ),
      'color' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/color/color.module',
        'basename' => 'color.module',
        'name' => 'color',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/color/color.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'ckeditor_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/ckeditor/tests/modules/ckeditor_test.module',
        'basename' => 'ckeditor_test.module',
        'name' => 'ckeditor_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/ckeditor/tests/modules/ckeditor_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'ckeditor' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/ckeditor/ckeditor.module',
        'basename' => 'ckeditor.module',
        'name' => 'ckeditor',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/ckeditor/ckeditor.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'breakpoint' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/breakpoint/breakpoint.module',
        'basename' => 'breakpoint.module',
        'name' => 'breakpoint',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/breakpoint/breakpoint.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'book_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/book/tests/modules/book_test/book_test.module',
        'basename' => 'book_test.module',
        'name' => 'book_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/book/tests/modules/book_test/book_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'book_breadcrumb_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/book/tests/modules/book_breadcrumb_test/book_breadcrumb_test.module',
        'basename' => 'book_breadcrumb_test.module',
        'name' => 'book_breadcrumb_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/book/tests/modules/book_breadcrumb_test/book_breadcrumb_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'book' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/book/book.module',
        'basename' => 'book.module',
        'name' => 'book',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/book/book.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'block_place' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/block_place/block_place.module',
        'basename' => 'block_place.module',
        'name' => 'block_place',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/block_place/block_place.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'block_content_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/block_content/tests/modules/block_content_test/block_content_test.module',
        'basename' => 'block_content_test.module',
        'name' => 'block_content_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/block_content/tests/modules/block_content_test/block_content_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'block_content' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/block_content/block_content.module',
        'basename' => 'block_content.module',
        'name' => 'block_content',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/block_content/block_content.info.yml',
        'schema_version' => '8300',
        'version' => '/',
      ),
      'block_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/block/tests/modules/block_test/block_test.module',
        'basename' => 'block_test.module',
        'name' => 'block_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/block/tests/modules/block_test/block_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'block' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/block/block.module',
        'basename' => 'block.module',
        'name' => 'block',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/block/block.info.yml',
        'schema_version' => '8003',
        'version' => '/',
      ),
      'big_pipe_test' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/big_pipe/tests/modules/big_pipe_test/big_pipe_test.module',
        'basename' => 'big_pipe_test.module',
        'name' => 'big_pipe_test',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/big_pipe/tests/modules/big_pipe_test/big_pipe_test.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'big_pipe' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/big_pipe/big_pipe.module',
        'basename' => 'big_pipe.module',
        'name' => 'big_pipe',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/big_pipe/big_pipe.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'basic_auth' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/basic_auth/basic_auth.module',
        'basename' => 'basic_auth.module',
        'name' => 'basic_auth',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/basic_auth/basic_auth.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'ban' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/ban/ban.module',
        'basename' => 'ban.module',
        'name' => 'ban',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/ban/ban.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'automated_cron' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/automated_cron/automated_cron.module',
        'basename' => 'automated_cron.module',
        'name' => 'automated_cron',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/automated_cron/automated_cron.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
      'aggregator' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/aggregator/aggregator.module',
        'basename' => 'aggregator.module',
        'name' => 'aggregator',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/aggregator/aggregator.info.yml',
        'schema_version' => '8200',
        'version' => '/',
      ),
      'action' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/modules/action/action.module',
        'basename' => 'action.module',
        'name' => 'action',
        'info' => '/var/aegir/platforms/d8md8v/core/modules/action/action.info.yml',
        'schema_version' => 0,
        'version' => '/',
      ),
    ),
    'themes' => 
    array (
      'stark.info' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/themes/stark/stark.info.yml',
        'basename' => 'stark.info.yml',
        'name' => 'stark.info',
        'info' => '/var/aegir/platforms/d8md8v/core/themes/stark/stark.info.yml',
      ),
      'stable.info' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/themes/stable/stable.info.yml',
        'basename' => 'stable.info.yml',
        'name' => 'stable.info',
        'info' => '/var/aegir/platforms/d8md8v/core/themes/stable/stable.info.yml',
      ),
      'seven.info' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/themes/seven/seven.info.yml',
        'basename' => 'seven.info.yml',
        'name' => 'seven.info',
        'info' => '/var/aegir/platforms/d8md8v/core/themes/seven/seven.info.yml',
      ),
      'twig.info' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/themes/engines/twig/twig.info.yml',
        'basename' => 'twig.info.yml',
        'name' => 'twig.info',
        'info' => '/var/aegir/platforms/d8md8v/core/themes/engines/twig/twig.info.yml',
      ),
      'classy.info' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/themes/classy/classy.info.yml',
        'basename' => 'classy.info.yml',
        'name' => 'classy.info',
        'info' => '/var/aegir/platforms/d8md8v/core/themes/classy/classy.info.yml',
      ),
      'bartik.info' => 
      array (
        'filename' => '/var/aegir/platforms/d8md8v/core/themes/bartik/bartik.info.yml',
        'basename' => 'bartik.info.yml',
        'name' => 'bartik.info',
        'info' => '/var/aegir/platforms/d8md8v/core/themes/bartik/bartik.info.yml',
      ),
    ),
    'platforms' => 
    array (
      'drupal' => 
      array (
        'short_name' => 'drupal',
        'version' => '8.4.0-alpha1',
        'description' => 'This platform is running Drupal 8.4.0-alpha1',
      ),
    ),
    'profiles' => 
    array (
      'standard' => 
      array (
        'name' => 'standard',
        'filename' => './core/profiles/standard/standard.profile',
        'info' => 'd/core/profiles/standard/standard.info.yml',
        'version' => '8.4.0-alpha1',
      ),
    ),
  ),
  'sites-all' => 
  array (
    'modules' => 
    array (
    ),
    'themes' => 
    array (
    ),
  ),
  'profiles' => 
  array (
    'standard' => 
    array (
      'modules' => 
      array (
      ),
      'themes' => 
      array (
      ),
    ),
  ),
);